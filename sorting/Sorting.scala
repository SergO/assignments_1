package sorting

/**
  * Created by sergo on 9.4.17.
  */
class Sorting {

}
object Sorting {
  type UserSeq = List[Int]

  /**
    * Main sorting function
    * */
  def extendedSort(mergeFn: (UserSeq, UserSeq) => UserSeq)(seq: UserSeq): UserSeq = {

    def sort(seq: UserSeq): UserSeq = {
      val (splitSeq1, splitSeq2) = this.split(seq)
      splitSeq1 match {
        case List() =>
          // if splitSeq1 is empty, seq also is empty
          List()
        case num1 :: splitSeq1Tail =>
          splitSeq1Tail match {
            case List() =>
              // if splitSeq1 has only one element, splitSeq2 will not have more than one element
              mergeFn(splitSeq1, splitSeq2)
            case _ =>
              // if splitSeq1 has more than one element, splitSeq2 will have at least one element
              splitSeq2 match {
                case List() =>
                  // splitSeq2 doesn't have any elements, this case will never happen
                  // but it avoids the warning "It would fail on the following input: Nil"
                  sort(splitSeq1)
                case num2 :: splitSeq2Tail =>
                  splitSeq2Tail match {
                    case List() =>
                      // splitSeq2 has only one element
                      mergeFn(sort(splitSeq1), splitSeq2)
                    case _ =>
                      // splitSeq2 has more than one element
                      mergeFn(sort(splitSeq1), sort(splitSeq2))
                  }
              }
          }
      }

    }

    sort(seq)
  }


  /**
    * Merges two sorted Lists into one sorted List
    */
  def merge(seq1: UserSeq, seq2: UserSeq): UserSeq = {
    seq1 match {
      case List() => seq2
      case num1 :: seq1Tail => seq2 match {
        case List() => seq1
        case num2 :: seq2Tail =>
          if (num1 < num2)
            num1 :: merge(seq1Tail, seq2)
          else
            num2 :: merge(seq1, seq2Tail)
      }
    }
  }


  /**
    * Merges two sorted Lists into one sorted List by tail recursion
    */
  def mergeT(seq1: UserSeq, seq2: UserSeq): UserSeq = {

    def acc(seq1: UserSeq, seq2: UserSeq, mergedSeq: UserSeq): (UserSeq, UserSeq, UserSeq) = {
      seq1 match {
        // empty
        case List() => seq2 match {
          // empty, empty
          case List() => (List(), List(), mergedSeq)
          // empty, not empty
          case num2 :: seq2Tail => acc(List(), seq2Tail, num2 :: mergedSeq)
        }
        // not empty
        case num1 :: seq1Tail => seq2 match {
          // not empty, empty
          case List() => acc(seq1Tail, List(), num1 :: mergedSeq)
          // not empty, not empty
          case num2 :: seq2Tail =>
            if (num1 < num2)
              acc(seq1Tail, seq2, num1 :: mergedSeq)
            else
              acc(seq1, seq2Tail, num2 :: mergedSeq)
        }
      }
    }

    val (_, _, result) = acc(seq1, seq2, List())

    result.reverse

  }


  /**
    * Splits List in two
    * */
  def split(origSeq: UserSeq): (UserSeq, UserSeq) = {
    def acc(origSeq: UserSeq, resSeq1: UserSeq, resSeq2: UserSeq, turn: Int): (UserSeq, UserSeq) = {
      origSeq match {
        case List() => (resSeq1, resSeq2)
        case _ =>
          if (turn == 1)
            acc(origSeq.tail, origSeq.head :: resSeq1, resSeq2, 2)
          else
            acc(origSeq.tail, resSeq1, origSeq.head :: resSeq2, 1)
      }
    }

    acc(origSeq, List(), List(), 1)
  }
}
