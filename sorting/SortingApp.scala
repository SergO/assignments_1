package sorting

import scala.util.Random
import java.io._

import Sorting.UserSeq

/**
  * Created by sergo on 9.4.17.
  */
object SortingApp {
  def main(args: Array[String]): Unit = {

    def generateListOfNumbers(size: Int): UserSeq = for (_ <- Range(1, size+1).toList) yield Random.nextInt(size)+1

    def saveToFile(fileName: String, data: String): Unit = {
      val pw = new PrintWriter(new File(fileName))
      pw.write(data)
      pw.close()
    }

    def seqIntToString(seq: UserSeq): String = {for (num <- seq) yield  "\n" + num.toString}.toString()

    val n = 5000

    println("Sorting list of %d random numbers".format(n))
    val generatedInputSeq1000 = generateListOfNumbers(n)
    val sortedSeq = Sorting.extendedSort(Sorting.merge)(generatedInputSeq1000)

    saveToFile("Sorting_%d.txt".format(n), seqIntToString(generatedInputSeq1000) + "\n\n" + seqIntToString(sortedSeq))

    println("OK")
    println()


    val N = 1000000

    println("Sorting list of %d random numbers by tail-recursive merging".format(N))
    val generatedInputSeq1000000 = generateListOfNumbers(N)
    val sortedSeqT = Sorting.extendedSort(Sorting.mergeT)(generatedInputSeq1000000)

    saveToFile("Sorting_%d.txt".format(N), seqIntToString(generatedInputSeq1000000)+"\n\n"+seqIntToString(sortedSeqT))

    println("OK")
    println()

  }
}
