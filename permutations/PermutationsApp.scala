package permutations

import java.io.{File, PrintWriter}

/**
  * Created by sergo on 8.4.17.
  */
object PermutationsApp {
  def main(args: Array[String]): Unit = {


    def saveToFile(fileName: String, data: String): Unit = {
      val pw = new PrintWriter(new File(fileName))
      pw.write(data)
      pw.close()
    }

    val n = 10

    val permutations = Permutations.permutations(n)

    saveToFile("Permutations_%d.txt".format(n),
      "Total permutations for n=%d: %d".format(n, permutations.size) +
        "\n\n" + {for (mut <- permutations) yield  "\n" + mut.toString()}
    )

  }
}
