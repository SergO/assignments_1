package permutations

/**
  * Created by SergO on 7.4.17.
  */

case class Permutation(initSeq: List[Int], initStack: List[Int], finalPermutation: List[Int])

class Permutations {

}
object Permutations {
  def permutations(n:Int): Seq[Seq[Int]] = {
    val initSeq = Range(1,n+1).toList
    val initPermutation = Permutation(initSeq, List(), List())

    def acc(permutation:Permutation, res: List[List[Int]]): List[List[Int]] = {
      permutation match {
        case Permutation(List(), List(), fMut) => fMut :: res
        case Permutation(List(), iStack, fMut) => res
        case Permutation(initNum :: initSeqTail, iStack, fMut) =>
          acc(Permutation(iStack ::: initSeqTail, List(), initNum :: fMut), res) :::
            acc(Permutation(initSeqTail, initNum :: iStack, fMut), res)
      }
    }

    acc(initPermutation, List())

  }
}
