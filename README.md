# Assignments #

## Implement a sorting function with the following signature: ##
```scala
def sort(seq: Seq[Int]): Seq[Int]
```
### Solution ###
**SortingApp.scala**,
**Sorting.scala**

Implemented a function `sort` (as inner function of `Sorting.extendedSort`) 
which sorts list of integers by 
**[Mergesort](https://en.wikipedia.org/wiki/Merge_sort)** algorithm.

Merging of sorted lists is implemented in two options: 
as a recursive function `Sorting.merge` 
and as a tail recursive function `Sorting.mergeT`.

We can choose a merge option by passing of corresponding function 
as a parameter of the function `Sorting.extendedSort`.

In case of using `Sorting.merge`, the program breaks down with the 
`java.lang.StackOverflowError` exception when initial sequence 
contains more than 4000 elements.

**sorting_output.tar.gz** contains results of sorting:

  * **Sorting_4000.txt** sorting of list of 4 000 elements with `Sorting.merge` method
  * **Sorting_1000000.txt** sorting of list of 1 000 000 elements 
  with the tail recursive method `Sorting.mergeT`

## implement a function that generates permutations of 1 to n with the following signature: ##
```scala
def permutations(n: Int): Seq[Seq[Int]]
```
### Solution ###
**PermutationsApp.scala**,
**Permutations.scala**

Tail recursive function is not implemented.

**permutations_output.tar.gz** contains results of generating of permutations:

  * **Permutations_5.txt** List of permutaions for `n=5` (number of permutations: 120)
  * **Permutations_10.txt** List of permutaions for `n=10` (number of permutations: 3 628 800)
